use image::io::Reader;
use image::DynamicImage;
use image::ImageError;
use log::error;
use std::collections::VecDeque;
use std::error::Error;
use std::io;
use std::path::PathBuf;
use winit::keyboard::KeyCode;

pub struct Images {
    images: VecDeque<PathImage>,
    image: Image,
}

enum PathImage {
    Path(PathBuf),
    Image(Image),
}

struct Image {
    path: String,
    image: DynamicImage,
}

impl Images {
    pub fn rotate(&mut self, key: KeyCode) -> Result<bool, Box<dyn Error>> {
        // A key press decides which side of the queue to pop from and push to.
        let (pop, push): (&dyn Fn(_) -> _, &dyn Fn(_, _)) = match key {
            KeyCode::ArrowLeft => (&VecDeque::pop_back, &VecDeque::push_front),
            KeyCode::ArrowRight => (&VecDeque::pop_front, &VecDeque::push_back),
            _ => return Err("An inappropriate key was pressed.".into()),
        };

        // The borrow checker won't allow &mut self.images in the next iteration of the loop,
        // nonlexical lifetimes are not implemented and self.images.pop_front/back() works.
        while let Some(new_image) = unsafe { pop(&mut *(&mut self.images as *mut _)) } {
            let new_image = match new_image {
                PathImage::Path(path) => {
                    let image = Reader::open(path.as_path())?
                        .with_guessed_format()?
                        .decode();
                    match image {
                        Err(ImageError::Unsupported(error)) => {
                            error!("{}: {error}", path.display());
                            continue;
                        }
                        _ => {
                            let path = path.to_string_lossy().to_string();
                            let image = image?;
                            Image { path, image }
                        }
                    }
                }
                PathImage::Image(image) => image,
            };
            let old_image = std::mem::replace(&mut self.image, new_image);
            push(&mut self.images, PathImage::Image(old_image));
            return Ok(true);
        }
        Ok(false)
    }

    pub fn path(&self) -> &str {
        self.image.path.as_str()
    }

    pub fn image(&self) -> &DynamicImage {
        &self.image.image
    }
}

fn walk(
    path: PathBuf,
    images: &mut VecDeque<PathImage>,
    depth: Option<u8>,
) -> Result<(), io::Error> {
    let metadata = path.symlink_metadata()?;
    if metadata.is_file() {
        let path = PathImage::Path(path);
        images.push_back(path)
    } else if metadata.is_dir() && depth != Some(0) {
        let depth = match depth {
            Some(depth) => Some(depth - 1),
            None => depth,
        };
        match path.read_dir() {
            Err(e) if e.kind() == io::ErrorKind::PermissionDenied => {
                error!("{}: {e}", path.display())
            }
            dir => {
                for dir_entry in dir? {
                    let path = dir_entry?.path();
                    walk(path, images, depth)?
                }
            }
        }
    }
    Ok(())
}

pub fn setup(paths: Vec<String>, depth: Option<u8>) -> Result<Images, Box<dyn Error>> {
    let mut images = VecDeque::new();
    for path in paths.iter().map(PathBuf::from) {
        walk(path, &mut images, depth)?;
    }
    while let Some(image) = images.pop_front() {
        match image {
            PathImage::Path(path) => match Reader::open(path.as_path()) {
                Err(e) if e.kind() == io::ErrorKind::PermissionDenied => {
                    error!("{}: {e}", path.display())
                }
                image => match image?.with_guessed_format() {
                    // TODO: handle ErrorKind::Uncategorized specifically.
                    Err(e) => error!("{}: {e}", path.display()),
                    Ok(image) => match image.decode() {
                        Ok(image) => {
                            let path = path.to_string_lossy().to_string();
                            let image = Image { path, image };
                            return Ok(Images { images, image });
                        }
                        Err(ImageError::Unsupported(error)) => {
                            error!("{}: {error}", path.display())
                        }
                        Err(error) => return Err(Box::new(error)),
                    },
                },
            },
            _ => return Err("Another image was already loaded.".into()),
        };
    }
    Err("No images were found, exiting program.".into())
}
