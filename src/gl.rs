#![allow(
    clippy::manual_non_exhaustive,
    clippy::too_many_arguments,
    clippy::missing_safety_doc,
    clippy::unused_unit,
    clippy::upper_case_acronyms,
    non_camel_case_types
)]

// gl_bindings.rs is generated in build.rs using https://crates.io/crates/gl_generator
include!(concat!(env!("OUT_DIR"), "/gl_bindings.rs"));
