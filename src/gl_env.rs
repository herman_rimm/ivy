use crate::gl;
use crate::Filter;
use glutin::display::{Display, GlDisplay};
use image::DynamicImage;
use log::{error, info};
use nalgebra::Matrix4;
use nalgebra::Vector3;
use std::ffi::CStr;

pub struct GLEnv {
    gl: gl::Gles2,
    transform: i32,
    matrix: Matrix4<f32>,
}

impl GLEnv {
    pub fn scale(&mut self, exp: i32, x: f32, y: f32) {
        let vector = Vector3::new(x, y, 0.0);

        self.matrix = if exp < 0 {
            self.matrix
                .append_scaling(2_f32.powi(exp))
                .append_translation(&-vector)
        } else {
            self.matrix
                .append_translation(&vector)
                .append_scaling(2_f32.powi(exp))
        };
        unsafe {
            self.gl
                .UniformMatrix4fv(self.transform, 1, gl::FALSE, self.matrix.as_ptr() as _);
        }
    }

    pub fn translate(&mut self, x: f32, y: f32) {
        self.matrix = self.matrix.append_translation(&Vector3::new(x, y, 0.0));
        unsafe {
            self.gl
                .UniformMatrix4fv(self.transform, 1, gl::FALSE, self.matrix.as_ptr() as _);
        }
    }

    pub fn resize(&mut self, (width, height): (i32, i32)) {
        unsafe {
            self.gl.Viewport(0, 0, width, height);
        }
    }

    pub fn draw(&self) {
        unsafe {
            self.gl.Clear(gl::COLOR_BUFFER_BIT);

            self.gl.DrawArrays(gl::TRIANGLE_STRIP, 0, 4);
        }
    }

    pub fn use_filter(&self, filter: Filter) {
        let scaling = match filter {
            Filter::Nearest => gl::NEAREST,
            Filter::Linear => gl::LINEAR,
        } as i32;

        unsafe {
            self.gl
                .TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, scaling);
            self.gl
                .TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, scaling);
        }
    }

    pub fn fit(&mut self, (image_width, image_height): (u32, u32), (width, height): (f32, f32)) {
        // The image fits the window so compress the most strechted side.
        let horizontal_ratio = image_width as f32 / width;
        let vertical_ratio = image_height as f32 / height;
        let scaling = if vertical_ratio < horizontal_ratio {
            Vector3::new(1.0, vertical_ratio / horizontal_ratio, 0.0)
        } else {
            Vector3::new(horizontal_ratio / vertical_ratio, 1.0, 0.0)
        };
        self.matrix = Matrix4::new_nonuniform_scaling(&scaling);

        unsafe {
            self.gl
                .UniformMatrix4fv(self.transform, 1, gl::FALSE, self.matrix.as_ptr() as _);
        }
    }

    pub fn load(&mut self, image: &DynamicImage) {
        let gl = &self.gl;

        // Determine texture properties from image.
        let width: i32 = image.width().try_into().unwrap();
        let height: i32 = image.height().try_into().unwrap();
        use image::ColorType::*;
        let (format, gl_type) = match image.color() {
            // Read luminance as red to swizzle to blue and green.
            L8 => (gl::RED, gl::UNSIGNED_BYTE),
            L16 => (gl::RED_INTEGER, gl::UNSIGNED_SHORT),
            // Read alpha as green to swizzle to alpha.
            La8 => (gl::RG, gl::UNSIGNED_BYTE),
            La16 => (gl::RG_INTEGER, gl::UNSIGNED_SHORT),
            Rgb8 => (gl::RGB, gl::UNSIGNED_BYTE),
            Rgb16 => (gl::RGB, gl::UNSIGNED_SHORT),
            Rgba8 => (gl::RGBA, gl::UNSIGNED_BYTE),
            Rgba16 => (gl::RGBA, gl::UNSIGNED_SHORT),
            Rgb32F => (gl::RGB, gl::FLOAT),
            Rgba32F => (gl::RGBA, gl::FLOAT),
            _ => {
                error!("unknown image color format: {:?}", &image.color());
                (gl::RGB, gl::UNSIGNED_BYTE)
            }
        };

        unsafe {
            gl.TexImage2D(
                gl::TEXTURE_2D,
                0,
                format as i32,
                width,
                height,
                0,
                format,
                gl_type,
                image.as_bytes().as_ptr() as _,
            );

            if image.color().has_alpha() {
                gl.Enable(gl::BLEND);
            } else {
                gl.Disable(gl::BLEND);
            }

            if image.color().has_color() {
                gl.TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_SWIZZLE_G, gl::GREEN as i32);
                gl.TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_SWIZZLE_B, gl::BLUE as i32);
                gl.TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_SWIZZLE_A, gl::ALPHA as i32);
            } else {
                gl.TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_SWIZZLE_G, gl::RED as i32);
                gl.TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_SWIZZLE_B, gl::RED as i32);
                if image.color().has_alpha() {
                    gl.TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_SWIZZLE_A, gl::GREEN as i32);
                }
            }
        }
    }
}
use std::ffi::CString;
pub fn setup(display: &Display) -> GLEnv {
    let gl = gl::Gles2::load_with(|ptr| {
        let ptr = CString::new(ptr).unwrap();
        display.get_proc_address(ptr.as_c_str()) as *const _
    });

    let version = unsafe {
        let data = CStr::from_ptr(gl.GetString(gl::VERSION) as *const _)
            .to_bytes()
            .to_vec();
        String::from_utf8(data).unwrap()
    };
    info!("OpenGL ES {}", version);

    unsafe {
        let mut viewport = vec![0; 4];
        gl.GetIntegerv(gl::VIEWPORT, viewport.as_mut_ptr());

        // setup program
        let vs = gl.CreateShader(gl::VERTEX_SHADER);
        gl.ShaderSource(
            vs,
            1,
            [VS_SRC.as_ptr() as *const _].as_ptr(),
            std::ptr::null(),
        );
        gl.CompileShader(vs);

        let fs = gl.CreateShader(gl::FRAGMENT_SHADER);
        gl.ShaderSource(
            fs,
            1,
            [FS_SRC.as_ptr() as *const _].as_ptr(),
            std::ptr::null(),
        );
        gl.CompileShader(fs);

        let program = gl.CreateProgram();
        gl.AttachShader(program, vs);
        gl.AttachShader(program, fs);
        gl.LinkProgram(program);
        gl.UseProgram(program);

        // setup attributes
        let mut vb = std::mem::zeroed();
        gl.GenBuffers(1, &mut vb);
        gl.BindBuffer(gl::ARRAY_BUFFER, vb);
        gl.BufferData(
            gl::ARRAY_BUFFER,
            (VERTEX_DATA.len() * std::mem::size_of::<f32>()) as gl::types::GLsizeiptr,
            VERTEX_DATA.as_ptr() as *const _,
            gl::STATIC_DRAW,
        );

        if gl.BindVertexArray.is_loaded() {
            let mut vao = std::mem::zeroed();
            gl.GenVertexArrays(1, &mut vao);
            gl.BindVertexArray(vao);
        }

        let vertex = gl.GetAttribLocation(program, b"vertex\0".as_ptr() as *const _);
        let texture = gl.GetAttribLocation(program, b"texture\0".as_ptr() as *const _);
        gl.VertexAttribPointer(
            vertex as gl::types::GLuint,
            2,
            gl::FLOAT,
            0,
            0,
            std::ptr::null(),
        );
        gl.VertexAttribPointer(
            texture as gl::types::GLuint,
            2,
            gl::FLOAT,
            0,
            0,
            (8 * std::mem::size_of::<f32>()) as *const () as *const _,
        );
        gl.EnableVertexAttribArray(vertex as gl::types::GLuint);
        gl.EnableVertexAttribArray(texture as gl::types::GLuint);

        let mut texture = std::mem::zeroed();
        gl.GenTextures(1, &mut texture);
        gl.BindTexture(gl::TEXTURE_2D, texture);

        gl.PixelStorei(gl::UNPACK_ALIGNMENT, 1);

        // Alpha value blends image with black background.
        gl.BlendFunc(gl::SRC_ALPHA, gl::ONE);
        gl.ClearColor(0.0, 0.0, 0.0, 1.0);

        let transform = gl.GetUniformLocation(program, b"transform\0".as_ptr() as *const _);
        let matrix = Matrix4::identity();
        GLEnv {
            gl,
            transform,
            matrix,
        }
    }
}

static VERTEX_DATA: [f32; 16] = [
    -1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0,
];

const VS_SRC: &[u8] = b"
#version 100
precision mediump float;

uniform mat4 transform;
attribute vec2 vertex;
attribute vec2 texture;
varying vec2 _texture;

void main() {
    gl_Position = transform * vec4(vertex, 0.0, 1.0);
    _texture = texture;
}
\0";

const FS_SRC: &[u8] = b"
#version 100
precision mediump float;

uniform sampler2D sampler;
varying vec2 _texture;

void main() {
    gl_FragColor = texture2D(sampler, _texture);
}
\0";
