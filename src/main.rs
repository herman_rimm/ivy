pub mod gl;
pub mod gl_env;
pub mod path_image;

use clap::{ArgEnum, Parser};
use glutin::config::ConfigTemplateBuilder;
use glutin::context::{ContextApi, ContextAttributesBuilder, GlProfile, NotCurrentGlContext};
use glutin::display::{GetGlDisplay, GlDisplay};
use glutin::surface::{GlSurface, SurfaceAttributesBuilder};
use glutin_winit::{ApiPreference, DisplayBuilder, GlWindow};
use winit::dpi::PhysicalPosition;
use winit::event::ElementState;
use winit::event::KeyEvent;
use winit::event::MouseButton;
use winit::event::MouseScrollDelta::LineDelta;
use winit::event::{Event, WindowEvent};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::keyboard::KeyCode;
use winit::keyboard::PhysicalKey;
use winit::window::WindowBuilder;
use image::GenericImageView;
use log::error;
use simplelog::*;
use std::error::Error;

/// image viewer
#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// File paths of images.
    #[clap(value_parser, default_value = ".")]
    file_paths: Vec<String>,

    /// Filtering method.
    #[clap(short, long, value_parser, default_value_t = Filter::Linear, arg_enum)]
    filter: Filter,

    /// Recursive search depth.
    #[clap(short, long, value_parser, default_value_t = 1)]
    depth: u8,

    /// Recurse with infinite search depth.
    #[clap(short, long, value_parser, default_value_t = false)]
    recursive: bool,
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ArgEnum)]
pub enum Filter {
    Linear,
    Nearest,
}

fn main() -> Result<(), Box<dyn Error>> {
    TermLogger::init(
        LevelFilter::Info,
        Config::default(),
        TerminalMode::Mixed,
        ColorChoice::Auto,
    )?;

    let args = Args::parse();
    let depth = if args.recursive {
        None
    } else {
        Some(args.depth)
    };
    let mut images = match path_image::setup(args.file_paths, depth) {
        Ok(images) => images,
        // Pass logger errors to stdout and other errors to the logger.
        Err(error) => return Ok(error!("{error}")),
    };

    let el = EventLoop::new()?;
    let config_template = ConfigTemplateBuilder::new();
    let window = WindowBuilder::new().with_title(images.path());
    let (window, config) = DisplayBuilder::new()
        .with_preference(ApiPreference::PreferEgl)
        .with_window_builder(Some(window))
        .build(&el, config_template, |mut configs| configs.next().unwrap())?;
    let window = window.unwrap();
    let display = config.display();
    let surface_attributes = window.build_surface_attributes(SurfaceAttributesBuilder::new());
    let surface = unsafe {
        display.create_window_surface(&config, &surface_attributes)?
    };
    let context_attributes = ContextAttributesBuilder::new()
        // See build.rs, use latest available version.
        .with_profile(GlProfile::Core)
        .with_context_api(ContextApi::Gles(None))
        .build(None);
    let context = unsafe {
        display.create_context(&config, &context_attributes)?
            .make_current(&surface)?
    };

    let mut gl_env = gl_env::setup(&display);
    let mut size = window.inner_size();
    let mut center_x = (size.width / 2) as f32;
    let mut center_y = (size.height / 2) as f32;
    let mut cursor_position = (0.0, 0.0);
    let mut button_press = false;

    gl_env.load(images.image());
    gl_env.fit(images.image().dimensions(), size.into());
    gl_env.use_filter(args.filter);
    el.run(move |event, window_target| {
        window_target.set_control_flow(ControlFlow::Wait);

        match event {
            Event::LoopExiting => (),
            Event::WindowEvent { event, .. } => match event {
                WindowEvent::KeyboardInput {
                    event:
                        KeyEvent {
                            state: ElementState::Pressed,
                            physical_key: PhysicalKey::Code(key),
                            ..
                        },
                    ..
                } => match key {
                    KeyCode::ArrowLeft | KeyCode::ArrowRight => {
                        match images.rotate(key) {
                            Ok(true) => {
                                window.set_title(images.path());
                                gl_env.load(images.image());
                            }
                            Ok(false) => (),
                            Err(error) => error!("{error}"),
                        }
                        gl_env.fit(images.image().dimensions(), size.into());
                        window.request_redraw();
                    }
                    _ => (),
                },
                WindowEvent::MouseInput {
                    state,
                    button: MouseButton::Left,
                    ..
                } => match state {
                    ElementState::Pressed => button_press = true,
                    ElementState::Released => button_press = false,
                },
                WindowEvent::CursorMoved {
                    position: PhysicalPosition { x, y },
                    ..
                } => {
                    if button_press {
                        let (x1, y1) = cursor_position;
                        gl_env.translate((x - x1) as f32 / center_x, (y1 - y) as f32 / center_y);
                        window.request_redraw();
                    }
                    cursor_position = (x, y);
                }
                WindowEvent::MouseWheel {
                    delta: LineDelta(_, y),
                    ..
                } => {
                    let (cursor_x, cursor_y) = cursor_position;
                    gl_env.scale(
                        y as i32,
                        (center_x - cursor_x as f32) / size.width as f32,
                        (cursor_y as f32 - center_y) / size.height as f32,
                    );
                    window.request_redraw();
                }
                WindowEvent::Resized(physical_size) => {
                    window.resize_surface(&surface, &context);
                    gl_env.resize(physical_size.into());

                    size = physical_size;
                    center_x = (size.width / 2) as f32;
                    center_y = (size.height / 2) as f32;
                }
                WindowEvent::CloseRequested => window_target.exit(),
                WindowEvent::RedrawRequested => {
                    gl_env.draw();
                    surface.swap_buffers(&context).unwrap();
                },
                _ => (),
            },
            _ => (),
        }
    })?;
    Ok(())
}
